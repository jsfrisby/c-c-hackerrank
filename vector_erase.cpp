// https://www.hackerrank.com/challenges/vector-erase/problem

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    vector<int> vect;
    int size;   // note that size will eventually change
    int num;
    int line3;
    int line4a, line4b;
    
    cin >> size;
    
    for (int i = 0; i < size; i++)
    {
        cin >> num;
        vect.push_back(num);
    }
    
    cin >> line3;
    
    vect.erase(vect.begin() + line3 - 1);
    
    cin >> line4a >> line4b;
    
    vect.erase(vect.begin() + line4a - 1, vect.begin() + line4b - 1);
    
    cout << vect.size() << endl;
    
    for (int i = 0; i < vect.size(); i++)
    {
        cout << vect[i] << " ";
    }
    
    cout << endl;
    
    return 0;
}
