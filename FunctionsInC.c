// https://www.hackerrank.com/challenges/functions-in-c/problem

#include <stdio.h>
/*
Add `int max_of_four(int a, int b, int c, int d)` here.
*/
int max_of_four(int a, int b, int c, int d){
    int f, s;
    f = max(a, b);
    s = max(c, d);
    return max(f, s);
}

// https://stackoverflow.com/a/3437484
int max(int num1, int num2){
    if (num1 > num2)
    {
        return num1;
    }

    return num2;
}


int main() {
    int a, b, c, d;
    scanf("%d %d %d %d", &a, &b, &c, &d);
    int ans = max_of_four(a, b, c, d);
    printf("%d", ans);

    return 0;
}
